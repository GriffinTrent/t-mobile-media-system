T-mobile project
======

Installing the project

1. composer install
2. (not needed if you have DB dump)app/console doctrine:database:create
3. (not needed if you have DB dump)app/console doctrine:schema:update --force
4. (not needed if you have DB dump)app/console fos:user:create to create super admin
5. (not needed if you have DB dump)app/console fos:user:promote and add role ROLE_SUPER_ADMIN
6. (not needed if you have DB dump)app/console fos:user:create to create store user
7. (not needed if you have DB dump)app/console fos:user:promote and add role ROLE_STORE
8. app/console assets:install
9. add write access to folder web/images recursively

E-mails

All configuration is done within the parameters.yml
```
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: ~
    mailer_api_key: ~
    mailer_port: 587
    order_notifications_email_from: info@symphonyno9.agency
    debug_email_address:
```

In DEV environment all mails are sent to the configured debug_email_address. In production environment there are two types of emails :

* New order notification - This email is sent to all admins in the system to their email addresses. The address can be changed in the user administration
* Order status change notification - this email is set to the client. The address is part of the user profile.