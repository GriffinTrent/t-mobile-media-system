<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use AppBundle\Entity\Order;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderStateChangeSubscriber implements EventSubscriber {

	private $oldOrderState;

	/** @var ContainerInterface */
	private $container;


	public function __construct(ContainerInterface $serviceContainer) {
		$this->container = $serviceContainer;
	}

	public function getSubscribedEvents() {
		return array(
			Events::preUpdate,
			Events::postUpdate,
			Events::postPersist
		);
	}

	private function isOrder($entity) {
		return $entity instanceof Order;
	}

	public function preUpdate(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($this->isOrder($entity)) {
			if($args->hasChangedField('state')) {
				$this->oldOrderState = $args->getOldValue('state');
			}
		}

	}

	public function postUpdate(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($this->isOrder($entity)) {
			if ($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				if (!empty($this->oldOrderState)) {
					//Send email to store
					$this->container->get('app.service.orderstatenotifier')->sendOrderStateChangeEmail($entity, $this->oldOrderState);
				}
			} else {
				$this->container->get('app.service.orderstatenotifier')->sendOrderChangeEmail($entity);
			}
		}
	}

	public function postPersist(LifecycleEventArgs $args) {
		$entity = $args->getEntity();
		if($this->isOrder($entity)) {
			//Send email to admin
			$this->container->get('app.service.orderstatenotifier')->sendNewOrderEmail($entity);
		}
	}

}