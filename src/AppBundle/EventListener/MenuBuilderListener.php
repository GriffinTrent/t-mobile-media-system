<?php
namespace AppBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MenuBuilderListener {

	/** @var AuthorizationChecker */
	private $authChecker;

	public function __construct(AuthorizationChecker $authChecker) {
		$this->authChecker = $authChecker;
	}

	public function addMenuItems(ConfigureMenuEvent $event) {
		$menu = $event->getMenu();

		if(!$this->authChecker->isGranted('ROLE_SUPER_ADMIN')) {
			$menu->removeChild('Stores');
			$menu->removeChild('Products');
			$menu->removeChild('Product Categories');
			$menu->removeChild('Text Versions');
			$menu->addChild('create_order', [
				'label' => 'Vytvořit Objednávku',
				'route' => 'admin_app_order_create',
			])->setExtras([
				'on_top' => true,
				'icon' => '<i class="fa fa-plus"></i>',
				'label_catalogue'=> 'SonataAdminBundle',
				'roles' => []
			]);
		}
		$menu->removeChild('sonata_media');
	}
}