<?php
namespace AppBundle\Admin;

use Doctrine\ORM\Query;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class StoreAdmin extends Admin {

	private function getUserQuery() {
		$em = $this->modelManager->getEntityManager('AppBundle\Entity\User');

		/**
		 * @var Query
		 */
		$query = $em->createQueryBuilder('u')
			->select('u')
			->from('AppBundle\Entity\User', 'u')
			->where('u.roles like :role')
			->setParameter('role', '%"ROLE_STORE"%')
			->orderBy('u.username','ASC');

		return $query;
	}

	protected function configureFormFields(FormMapper $formMapper) {

		$formMapper
			->add('name')
			->add('city')
			->add('user', 'sonata_type_model', [
				'query' => $this->getUserQuery(),
				'btn_add' => false
			])
			->add('disabled')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('city', null, ['show_filter' => true])
			->add('name', null, ['show_filter' => true])
			->add('user', null, ['show_filter' => true])
			->add('user.email', null, ['show_filter' => true])
			->add('disabled')
		;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->addIdentifier('name')
			->addIdentifier('city')
			->add('user')
			->add('user.email')
			->add('disabled')
			->add('_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => []
				]
			]);
		;
	}

	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->add('id')
			->add('name')
			->add('city')
			->add('user')
			->add('disabled')
		;
	}
}