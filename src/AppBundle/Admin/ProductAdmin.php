<?php
namespace AppBundle\Admin;

use Doctrine\ORM\Query;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ProductAdmin extends Admin {


	protected function configureFormFields(FormMapper $formMapper) {

		$formMapper
			->add('name')
			->add('productCategory')
			//I just dont know which of these two is better for our use case, the second one is more complex
			->add('image', 'sonata_media_type', ['provider' => 'sonata.media.provider.image', 'context'  => 'products'])
			//->add('image', 'sonata_type_model_list', [], ['link_parameters' => array('context' => 'products')]);
			->add('disabled')

		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('productCategory', null, ['show_filter' => true])
			->add('disabled', null, ['show_filter' => true])
		;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->addIdentifier('name')
			->add('productCategory')
			->add('disabled')
			->add('_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => []
				]
			]);
		;
	}

	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->add('id')
			->add('name')
			->add('productCategory')
			->add('disabled')
		;
	}

}