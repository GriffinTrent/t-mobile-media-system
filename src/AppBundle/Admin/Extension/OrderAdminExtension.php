<?php

namespace AppBundle\Admin\Extension;


use AppBundle\Entity\Order;
use AppBundle\Entity\Product;
use AppBundle\Entity\TextVersion;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class OrderAdminExtension extends AbstractAdminExtension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

	/**
	 * @var AuthorizationChecker
	 */
	private $authChecker;



	/**
     * OrderAdminExtension constructor.
     */
    public function __construct(EntityManager $entityManager, AuthorizationChecker $checker)
    {
        $this->entityManager = $entityManager;
        $this->authChecker = $checker;
    }

    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('product', 'product');
        $formMapper->get('product')->addModelTransformer(new ProductToNumberTransformer($this->entityManager));
        $formMapper->get('product')->setErrorBubbling(true);
        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (empty($data['product_category'])) {
                $data['product'] = null;
                $data['textVersion'] = null;
            }
            unset($data['product_category']);
            $event->setData($data);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function validate(AdminInterface $admin, ErrorElement $errorElement, $object)
    {
        /** @var Order $order */
        $order = $object;
        parent::validate($admin, $errorElement, $object);

        if (!$this->authChecker->isGranted('ROLE_SUPER_ADMIN') && in_array($order->getState(), Order::getCompletedStates())) {
            $errorElement->addViolation(
                sprintf('Objednávka je již dokončená a není možné ji upravit.')
            );
        }

        if (!empty($order->getProduct()) && $order->getProduct()->isDisabled()) {
            $errorElement->addViolation(sprintf('Produkt "%s" již není možné objednat, vyberte prosím jiný.', $order->getProduct()->getName()));
        }
        if (!empty($order->getTextVersion()) && ($order->getTextVersion()->getProduct() !== $order->getProduct())) {
            $productTextVersions = $order->getProduct()->getTextVersions()->toArray();
            $allowedTextVersions = array_map(function(TextVersion $textVersion) {
                return $textVersion->getName();
            }, $productTextVersions);
            $errorElement->addViolation(
                sprintf(
                    'Textovou variantu "%s" není možné vybrat k produktu "%s". Tato textová varianta je platná pro produkt "%s". Vyberte prosím textovou variantu, pro zvolený produkt. Dostupné možnosti jsou "%s"',
                        $order->getTextVersion()->getName(),
                        $order->getProduct()->getName(),
                        $order->getTextVersion()->getProduct()->getName(),
                        implode(', ', $allowedTextVersions)
                )
            );
        }
        if ($order->getStore()->isDisabled()) {
            $errorElement->addViolation(
                sprintf(
                    'Na prodejnu "%s" již není možné objednat materiály. Prosím vyberte jinou prodejnu.',
                    $order->getStore()->getName()
                )
            );
        }
    }


}

class ProductToNumberTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (product) to a string (number).
     *
     * @param  Product|null $product
     * @return string
     */
    public function transform($product)
    {
        if (null === $product) {
            return '';
        }

        return $product->getId();
    }

    /**
     * Transforms a string (number) to an object (product).
     *
     * @param  string $productId
     *
     * @return Product|null
     * @throws TransformationFailedException if object (product) is not found.
     */
    public function reverseTransform($productId)
    {
        // no product number? It's required so transformation error
        if (!$productId) {
            return null;
        }

        $product = $this->manager
            ->getRepository('AppBundle:Product')
            // query for the product with this id
            ->find($productId)
        ;

        if (null === $product) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(
                sprintf(
                    'Produkt "%s" neexistuje, prosím vyberte platný produkt!',
                    $productId
                )
            );
        }

        return $product;
    }
}
