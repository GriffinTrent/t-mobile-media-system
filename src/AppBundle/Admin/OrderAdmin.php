<?php
namespace AppBundle\Admin;

use AppBundle\Entity\Order;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class OrderAdmin extends Admin {

	const DATE_FORMAT = "d.m. Y";

	protected function getUser() {
		return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
	}

	protected function isStoreRole() {
		/**
		 * @var AuthorizationChecker
		 */
		$authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
		return $authChecker->isGranted('ROLE_STORE') && !$authChecker->isGranted('ROLE_SUPER_ADMIN');
	}

	protected function getStoreQuery() {
		$em = $this->modelManager->getEntityManager('AppBundle\Entity\Store');

		/**
		 * @var QueryBuilder
		 */
		$query = $em->createQueryBuilder('s')
			->select('s')
			->from('AppBundle\Entity\Store', 's')
			->orderBy('s.name','ASC');
		if($this->isStoreRole()) {
			$query->andWhere('s.user = :user')
				->setParameter(
					'user',
					$this->getUser()
				);
		}
		return $query;
	}

	protected function getProductQuery() {
		$em = $this->modelManager->getEntityManager('AppBundle\Entity\Product');

		/**
		 * @var Query
		 */
		$query = $em->createQueryBuilder('p')
			->select('p')
			->from('AppBundle\Entity\Product', 'p')
			->orderBy('p.name','ASC');
		if($this->isStoreRole()) {
			$query->andWhere('p.disabled = :disabled')
				->setParameter(
					'disabled',
					false
				);
		}
		return $query;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);
		if($this->isStoreRole()) {
			$query->innerJoin($query->getRootAliases()[0] . '.store', 's');
			$query->andWhere('s.user = :user');
			$query->andWhere($query->getRootAliases()[0] . '.state NOT IN (:state)');
			$query->setParameter('state', Order::getCompletedStates());
			$query->setParameter('user', $this->getUser());
		}
		return $query;
	}

	protected function createStoreForm(FormMapper $formMapper) {
		$formMapper
			->add(
				'store',
				'sonata_type_model',
				[
					'query' => $this->getStoreQuery(),
					'help' => 'store_field_help_text'
				]
			)
			->add(
				'product',
				'sonata_type_model',
				[
					'query' => $this->getProductQuery(),
					'help' => 'store_field_help_text'
				]
			)
			->add(
				'textVersion',
				null,
				[
					'help' => 'text_version_field_help_text',
					'multiple' => false,
					'expanded' => true,
				]
			)
			->add(
				'quantity',
				null,
				['help' => 'quantity_field_help_text']
			)
			->add(
				'height',
				null,
				['help' => 'height_field_help_text']
			)
			->add(
				'width',
				null,
				['help' => 'width_field_help_text']
			)
			->add(
				'customText',
				null,
				['help' => 'custom_text_field_help_text']
			)
			->add(
				'comment',
				null,
				['help' => 'comment_field_help_text']
			)
			//I just dont know which of these two is better for our use case, the second one is more complex
			->add(
				'image',
				'sonata_media_type',
				[
					'provider' => 'sonata.media.provider.image',
					'context'  => 'orders',
					'help' => 'image_field_help_text'
				]
			)
			//->add('image', 'sonata_type_model_list', [], ['link_parameters' => array('context' => 'products')]);
			->add(
				'delivery',
				'choice',
				[
					'choices' => Order::getDeliveryChoices(),
					'help' => 'delivery_field_help_text'
				]
			)
			->add(
				'address',
				null,
				['help' => 'address_field_help_text']
			)
			->add(
				'storePays',
				null,
				['help' => 'store_pays_field_help_text']
			)
		;
	}

	protected function createAdminForm(FormMapper $formMapper) {
		$formMapper
			->add(
				'store',
				'sonata_type_model',
				['query' => $this->getStoreQuery()]
			)
			->add('product')
			->add(
				'textVersion',
				null,
				[
					'help' => 'text_version_field_help_text',
					'multiple' => false,
					'expanded' => true,
				]
			)
			->add('quantity')
			->add('height')
			->add('width')
			->add('customText')
			->add('comment')
			//I just dont know which of these two is better for our use case, the second one is more complex
			->add('image', 'sonata_media_type', ['provider' => 'sonata.media.provider.image', 'context'  => 'orders'])
			//->add('image', 'sonata_type_model_list', [], ['link_parameters' => array('context' => 'products')]);
			->add('delivery', 'choice', [
				'choices' => Order::getDeliveryChoices()
			])
			->add('address')
			->add('adminNote')
			->add('storePays')
			->add('state', 'choice', [
				'choices' => Order::getStateChoices()
			])
			//I had to set manually required to false because datepicker is required by default, i dont know why
			->add('printSubmitted', 'sonata_type_date_picker', ['required' => false])
			->add('printDelivered', 'sonata_type_date_picker', ['required' => false])
			->add('orderDate', 'sonata_type_date_picker', ['required' => false])
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		//Admin sees different fields than Store
        if ($this->id($this->getSubject())) {
            $formMapper->add('action', 'hidden', [
                'data' => 'EDIT',
                'mapped' => false,
            ]);
        } else {
            $formMapper->add('action', 'hidden', [
                'data' => 'CREATE',
                'mapped' => false,
            ]);
        }
		if($this->isStoreRole()) {
			$this->createStoreForm($formMapper);
		} else {
			$this->createAdminForm($formMapper);
		}
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		if(!$this->isStoreRole()) {
			$datagridMapper
				->add(
					'id',
					null,
					['show_filter' => true]
				)
				->add(
					'product',
					null,
					['show_filter' => true]
				)
				->add(
					'store',
					null,
					['show_filter' => true]
				)
				->add(
					'store.user',
					null,
					['show_filter' => true]
				)
				->add(
					'orderDate',
					'doctrine_orm_date_range',
					[
						'show_filter' => true,
						'field_type' => 'sonata_type_date_range_picker'
					]
				)
				->add(
					'state',
					null,
					['show_filter' => true],
					'choice',
					['choices' => Order::getStateChoices()]
				);
		}
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
			->add('product.productCategory', 'html')
			->add('product')
			->add('textVersion')
			->add('orderDate', null, ['format' => self::DATE_FORMAT])
			->add('printSubmitted', null, ['format' => self::DATE_FORMAT])
			->add('printDelivered', null, ['format' => self::DATE_FORMAT])
			->add('state')
			->add('store')
			->add('_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => [],
					'clone' => [
						'template' => 'AppBundle:OrderCRUD:list__action_reorder.html.twig'
					]
				]
			]);

		;
	}

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('id')
			->add('product')
			->add('store')
			->add('orderDate')
			->add('state')
		;
	}

	protected function configureRoutes(RouteCollection $collection) {
		$collection->add('reorder', $this->getRouterIdParameter().'/reorder');
	}
}
