<?php
namespace AppBundle\Admin;

use Doctrine\ORM\Query;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class TextVersionAdmin extends Admin {


	protected function configureFormFields(FormMapper $formMapper) {

		$formMapper
			->add('name')
			->add('product')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('product', null, ['show_filter' => true])
		;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->addIdentifier('name')
			->add('product')
			->add('_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => []
				]
			]);
		;
	}

	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->add('id')
			->add('name')
		;
	}
}