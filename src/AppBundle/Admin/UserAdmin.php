<?php
namespace AppBundle\Admin;

use AppBundle\Entity\Store;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;

class UserAdmin extends SonataUserAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper->remove('facebookUid');
        $formMapper->remove('facebookName');
        $formMapper->remove('twitterUid');
        $formMapper->remove('twitterName');
        $formMapper->remove('gplusUid');
        $formMapper->remove('gplusName');
        $formMapper->remove('dateOfBirth');
        $formMapper->remove('website');
        $formMapper->remove('biography');
        $formMapper->remove('gender');
        $formMapper->remove('locale');
        $formMapper->remove('timezone');
        $formMapper->tab('User')->with('General')
            ->add('stores', 'entity', [
            'class' => Store::class,
            'multiple' => true
        ])->end()->end();
        $formMapper->removeGroup('Groups', 'Security');
        $formMapper->removeGroup('Roles', 'Security');
        $formMapper->removeGroup('Keys', 'Security');
        $roles = [
            'ROLE_STORE' => 'Obchod',
            'ROLE_SUPER_ADMIN' => 'Administrátor',
        ];
        $formMapper->tab('User')->with('General')->add('realRoles', 'choice', [
            'choices' => $roles,
            'expanded' => true,
            'multiple' => true
        ])->end()->end();
        $formMapper->removeGroup('Status', 'Security', true);
        $formMapper->tab('User')->with('Status')
            ->add('locked', 'checkbox', [
                'required' => false
            ])
            ->add('expired', 'checkbox', [
                'required' => false
            ])
            ->add('enabled', 'checkbox', [
                'required' => false
            ])
            ->add('credentialsExpired', 'checkbox', [
                'required' => false
            ])
        ->end()->end();
    }
}
