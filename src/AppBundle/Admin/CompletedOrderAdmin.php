<?php
namespace AppBundle\Admin;

use AppBundle\Entity\Order;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CompletedOrderAdmin extends OrderAdmin {
	protected $baseRouteName = 'completed_orders';
	protected $baseRoutePattern = 'completed_orders';

	public function createQuery($context = 'list') {

		$query = $this->getModelManager()->createQuery(Order::class);
		foreach ($this->extensions as $extension) {
			$extension->configureQuery($this, $query, $context);
		}

		if($this->isStoreRole()) {
			$query->innerJoin($query->getRootAliases()[0] . '.store', 's');
			$query->andWhere('s.user = :user');
			$query->setParameter('user', $this->getUser());
		}

		$query->andWhere($query->getRootAliases()[0] . '.state IN (:state)');
		$query->setParameter('state', Order::getCompletedStates());
		return $query;
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('id')
			->add('product.productCategory')
			->add('product')
			->add('orderDate', null, ['format' => self::DATE_FORMAT])
			->add('state')
			->add('store')

		;
		if($this->isStoreRole()) {
			$listMapper->add('_action', null, [
				'actions' => [
					'show' => [],
					'clone' => [
						'template' => 'AppBundle:OrderCRUD:list__action_reorder.html.twig'
					]
				]
			]);
		} else {
			$listMapper->add('_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => [],
					'clone' => [
						'template' => 'AppBundle:OrderCRUD:list__action_reorder.html.twig'
					]
				]
			]);
		}
	}

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('store')
			->add('product'
				, null, [
					'template' => 'AppBundle:OrderCRUD:product_show_field.html.twig'
				]
			)
			->add('textVersion')
			->add('quantity')
			->add('height')
			->add('width')
			->add('customText')
			->add('comment')
			->add(
				'image', null, [
					'template' => 'AppBundle:OrderCRUD:image_show_field.html.twig'
				]
			)
			->add('delivery')
			->add('address')
			->add('storePays')
			->add('state')
			->add('orderDate', null, ['format' => self::DATE_FORMAT])
		;
	}
}
