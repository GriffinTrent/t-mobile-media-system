<?php

namespace AppBundle\Entity;

use AppBundle\Exception\InvalidChoiceException;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\TextVersion;
use AppBundle\Entity\Product;

/**
 * ProductCategory
 *
 * @ORM\Table(name="`order`")
 * @ORM\Entity
 */
class Order
{
	const STATE_CONFIRMING = "Čeká na potvrzení";
	const STATE_IN_PROGRESS = "Pracuje se na tom";
	const STATE_IS_DONE = "Vyřízeno";
	const STATE_CANCELED = "Zrušeno";
	const STATE_DECLINED = "Zamítnuto";

	const DELIVERY_PRINT_DATA = "Dodat pouze tisková data";
	const DELIVERY_PRINTED = "Dodat vytištěná data";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Store")
	 * @ORM\JoinColumn(name="store_id", referencedColumnName="id", nullable=false)
	 */
	private $store;

	/**
	 * @var integer
	 * @ORM\Column(name="quantity", type="integer", nullable=false)
	 */
	private $quantity;

	/**
	 * @var integer
	 * @ORM\Column(name="height", type="string", nullable=false)
	 */
	private $height;

	/**
	 * @var integer
	 * @ORM\Column(name="width", type="string", nullable=false)
	 */
	private $width;

	/**
	 * @ORM\Column(name="print_submitted", type="date", nullable=true)
	 */
	private $printSubmitted;

	/**
	 * @ORM\Column(name="print_delivered", type="date", nullable=true)
	 */
	private $printDelivered;

	/**
	 * @ORM\Column(name="order_date", type="date", nullable=false)
	 */
	private $orderDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="state", type="string", length=255, nullable=true)
	 */
	private $state;

	/**
	 * @ORM\ManyToOne(targetEntity="TextVersion")
	 * @ORM\JoinColumn(name="text_version_id", referencedColumnName="id", nullable=true)
	 */
	private $textVersion;

	/**
	 * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
	 */
	private $product;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="custom_text", type="text", nullable=true)
	 */
	private $customText;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="comment", type="text", nullable=true)
	 */
	private $comment;

	/**
	 * @var string
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"})
	 */
	private $image;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="delivery", type="string", length=255, nullable=false)
	 */
	private $delivery;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="text", nullable=true)
	 */
	private $address;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="admin_note", type="text", nullable=true)
	 */
	private $adminNote;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="store_pays", type="boolean", nullable=false)
	 */
	private $storePays = false;

	public function __construct() {
		$this->orderDate = new \DateTime();
		$this->state = self::STATE_CONFIRMING;
	}

	public static function getStateChoices() {
		return [
			self::STATE_CONFIRMING => self::STATE_CONFIRMING,
			self::STATE_IN_PROGRESS => self::STATE_IN_PROGRESS,
			self::STATE_IS_DONE => self::STATE_IS_DONE,
			self::STATE_CANCELED => self::STATE_CANCELED,
			self::STATE_DECLINED => self::STATE_DECLINED
		];
	}

	public static function getCompletedStates() {
		return [
			self::STATE_IS_DONE,
			self::STATE_CANCELED,
			self::STATE_DECLINED
		];
	}

	public static function getDeliveryChoices() {
		return [
			self::DELIVERY_PRINT_DATA => self::DELIVERY_PRINT_DATA,
			self::DELIVERY_PRINTED => self::DELIVERY_PRINTED
		];
	}


	/**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


	/**
	 * @return Store
	 */
	public function getStore() {
		return $this->store;
	}

	/**
	 * @param Store $store
	 */
	public function setStore(Store $store) {
		$this->store = $store;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param mixed $quantity
	 */
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}

	/**
	 * @return mixed
	 */
	public function getPrintSubmitted() {
		return $this->printSubmitted;
	}

	/**
	 * @param mixed $printSubmitted
	 */
	public function setPrintSubmitted($printSubmitted) {
		$this->printSubmitted = $printSubmitted;
	}

	/**
	 * @return mixed
	 */
	public function getPrintDelivered() {
		return $this->printDelivered;
	}

	/**
	 * @param mixed $printDelivered
	 */
	public function setPrintDelivered($printDelivered) {
		$this->printDelivered = $printDelivered;
	}

	/**
	 * @return string
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * @return string
	 */
	public function getHeight() {
		return $this->height;
	}

	/**
	 * @param string $height
	 */
	public function setHeight($height) {
		$this->height = $height;
	}

	/**
	 * @return int
	 */
	public function getWidth() {
		return $this->width;
	}

	/**
	 * @param int $width
	 */
	public function setWidth($width) {
		$this->width = $width;
	}

	/**
	 * @return TextVersion
	 */
	public function getTextVersion() {
		return $this->textVersion;
	}

	/**
	 * @param TextVersion $textVersion
	 */
	public function setTextVersion(TextVersion $textVersion = NULL) {
		$this->textVersion = $textVersion;
	}

	/**
	 * @return Product
	 */
	public function getProduct() {
		return $this->product;
	}

	/**
	 * @param Product $product
	 */
	public function setProduct(Product $product = NULL) {
		$this->product = $product;
	}

	/**
	 * @return string
	 */
	public function getCustomText() {
		return $this->customText;
	}

	/**
	 * @param string $customText
	 */
	public function setCustomText($customText) {
		$this->customText = $customText;
	}

	/**
	 * @return string
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment) {
		$this->comment = $comment;
	}

	/**
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param string $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	 * @return string
	 */
	public function getDelivery() {
		return $this->delivery;
	}

	/**
	 * @param string $state
	 */
	public function setState($state) {
		$this->validateChoices(self::getStateChoices(), $state);
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress($address) {
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getAdminNote() {
		return $this->adminNote;
	}

	/**
	 * @param string $adminNote
	 */
	public function setAdminNote($adminNote) {
		$this->adminNote = $adminNote;
	}

	/**
	 * @return mixed
	 */
	public function getOrderDate() {
		return $this->orderDate;
	}

	/**
	 * @param mixed $orderDate
	 */
	public function setOrderDate($orderDate) {
		$this->orderDate = $orderDate;
	}

	/**
	 * @return bool
	 */
	public function isStorePays() {
		return $this->storePays;
	}

	/**
	 * @param bool $storePays
	 */
	public function setStorePays($storePays) {
		$this->storePays = $storePays;
	}

	/**
	 * @param string $delivery
	 */
	public function setDelivery($delivery) {
		$this->validateChoices(self::getDeliveryChoices(), $delivery);
		$this->delivery = $delivery;
	}

	private function validateChoices($choices, $value) {
		if(!in_array($value, $choices)) {
			throw new InvalidChoiceException("Invalid choice ".$value);
		}
	}

	public function __toString() {
		return "Objednávka #".$this->getId();
	}

}
