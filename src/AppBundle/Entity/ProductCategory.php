<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ProductCategory
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductCategoryRepository")
 */
class ProductCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

	/**
	 *
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="productCategory")
	 */
	private $products;

	public function __construct() {
		$this->prodcts = new ArrayCollection();
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

	/**
	 * @param Product $product
	 */
	public function addProduct(Product $product) {
		$this->products[] = $product;
	}

	/**
	 * @param Product $product
	 */
	public function removeProduct(Product $product) {
		$this->products->removeElement($product);
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection|Product[]
	 */
	public function getProducts() {
		return $this->products;
	}

	public function __toString() {
		return $this->getName();
	}

}
