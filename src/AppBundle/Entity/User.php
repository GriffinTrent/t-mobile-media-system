<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;

/**
 * User entity representing user of the system
 *
 * @ORM\Table(name="fos_user_user")
 * @ORM\Entity
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Store", mappedBy="user")
     *
     * @var Store[]|ArrayCollection
     */
    private $stores;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->stores = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Store[]|ArrayCollection
     */
    public function getStores()
    {
        return $this->stores->toArray();
    }

    public function addStore(Store $store) {
        if (!$this->stores->contains($store)) {
            $this->stores->add($store);
        }

        return $this;
    }

    public function removeStore(Store $store) {
        if ($this->stores->contains($store)) {
            $this->stores->remove($store);
        }

        return $this;
    }
}
