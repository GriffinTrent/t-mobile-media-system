<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Store
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Product {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
	 * @ORM\JoinColumn(name="product_category_id", referencedColumnName="id", nullable=false)
	 */
	private $productCategory;

	/**
	 * @ORM\OneToMany(targetEntity="TextVersion", mappedBy="product")
	 */
	private $textVersions;

	/**
	 * @var string
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"})
	 */
	private $image;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="disabled", type="boolean", nullable=false)
	 */
	private $disabled = false;

	public function __construct() {
		$this->textVersions = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
	* Set name
	*
	* @param string $name
	*
	* @return Product
	*/
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return ProductCategory
	 */
	public function getProductCategory() {
		return $this->productCategory;
	}

	/**
	 * @param ProductCategory $productCategory
	 */
	public function setProductCategory(ProductCategory $productCategory) {
		$this->productCategory = $productCategory;
	}

	/**
	 * @param TextVersion $textVersion
	 */
	public function addTextVersion(TextVersion $textVersion) {
		$this->textVersions[] = $textVersion;
	}

	/**
	 * @param TextVersion $textVersion
	 */
	public function removeTextVersion(TextVersion $textVersion) {
		$this->textVersions->removeElement($textVersion);
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection|TextVersion[]
	 */
	public function getTextVersions() {
		return $this->textVersions;
	}


	public function __toString() {
		return $this->getName();
	}

	/**
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param string $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	 * @return bool
	 */
	public function isDisabled() {
		return $this->disabled;
	}

	/**
	 * @param bool $disabled
	 */
	public function setDisabled($disabled) {
		$this->disabled = $disabled;
	}

}

