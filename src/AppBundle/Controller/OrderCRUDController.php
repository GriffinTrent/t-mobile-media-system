<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Order;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class OrderCRUDController extends Controller {

	private function checkOwnership($id) {
		if(!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			$id = $this->getRequest()->get($this->admin->getIdParameter());
			/** @var Order $object */
			$object = $this->admin->getObject($id);
			if ($object && $object->getStore()->getUser()->getId() != $this->getUser()->getId()) {
				throw $this->createAccessDeniedException();
			}
		}
	}

	private function checkState($id) {
		/** @var Order $object */
		$object = $this->admin->getObject($id);
		if(!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')
			&&  in_array($object->getState(), Order::getCompletedStates())) {
			throw $this->createAccessDeniedException();
		}
	}

	public function editAction($id = null) {
		$this->checkOwnership($id);
		$this->checkState($id);
		return parent::editAction($id);
	}

	public function showAction($id = null) {
		$this->checkOwnership($id);
		return parent::showAction($id);
	}

	public function deleteAction($id = null) {
		$this->checkOwnership($id);
		return parent::deleteAction($id);
	}

    /**
     * Reorder action create an order from previous order.
     *
     * @return Response
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function reorderAction($id)
    {
        $orderRepository = $this->get('doctrine.orm.default_entity_manager')->getRepository('AppBundle:Order');
        $previousOrder = $orderRepository->find($id);
        if (empty($previousOrder)) {
            $this->addFlash('error', sprintf(
                'Objednávka s id = "%s" nebyla nalezena. Není možné ji zopakovat.',
                $id
            ));
            return $this->redirectToRoute('admin_app_order_list');
        }
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $this->admin->checkAccess('show');

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->render(
                'SonataAdminBundle:CRUD:select_subclass.html.twig',
                array(
                    'base_template' => $this->getBaseTemplate(),
                    'admin' => $this->admin,
                    'action' => 'create',
                ),
                null,
                $request
            );
        }

        $object = $this->admin->getNewInstance();

        $preResponse = $this->preCreate($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($previousOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->admin->checkAccess('create', $object);

                try {
                    $object = $this->admin->create($object);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                        ), 200, array());
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->trans(
                            'flash_create_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_create_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $formView = $form->createView();
        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form' => $formView,
            'object' => $object,
        ), null);
    }

    /**
     * Sets the admin form theme to form view. Used for compatibility between Symfony versions.
     *
     * @param FormView $formView
     * @param string   $theme
     */
    private function setFormTheme(FormView $formView, $theme)
    {
        $twig = $this->get('twig');

        try {
            $twig
                ->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')
                ->setTheme($formView, $theme);
        } catch (\Twig_Error_Runtime $e) {
            // BC for Symfony < 3.2 where this runtime not exists
            $twig
                ->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')
                ->renderer
                ->setTheme($formView, $theme);
        }
    }
}
