<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\Product;
use AppBundle\Entity\ProductCategory;
use AppBundle\Service\ProductsService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{

    /**
     * @var ProductsService
     */
    private $productsService;

    /**
     * Lazy initialized set of {@see ProductCategory} entity.
     *
     * @var ProductCategory[]
     */
    private $productsSet;

    /**
     * ProductType constructor.
     *
     * @param ProductsService $productsService
     */
    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $products = [];
        /** @var ProductCategory $item */
        foreach ($this->getProductSet() as $item) {
            $p = $item->getProducts() ? $item->getProducts()->toArray() : [];
            $productInCategory = array_combine(
                array_map(function (Product $product) { return $product->getId(); }, $p),
                array_map(function (Product $product) { return $product->getName(); }, $p)
            );
            $products = $products + $productInCategory;
        }
        $resolver->setDefaults(
            [
                'choices' => $products,
                'expanded' => true,
                'multiple' => false,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['categories'] = $this->getProductSet();
        $view->vars['selectedCategory'] = $this->getSelectedCategory($form->getNormData());
        $view->vars['textVersionMapping'] = $this->getTextVersionMappings();
    }

    /**
     * Lazy initialize and return the initialized set of {@see ProductCategory} entity.
     *
     * @return ProductCategory[]
     */
    private function getProductSet()
    {
        if (is_null($this->productsSet)) {
            $this->productsSet = $this->productsService->getAllProductsGroupedByCategory();
            $productCategory = new ProductCategory();
            $productCategory->setName('Jiné');
            $this->productsSet[] = $productCategory;
        }

        return $this->productsSet;
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'product';
    }

    /**
     * Return parent FormType name
     *
     * @return string
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * Return Id of {@link ProductCategory} entity based on the {@see Product::id}
     *
     * @param int|null $selectedProductId Id of the Product entity
     *
     * @return int Id of selected category by selected product id
     */
    private function getSelectedCategory($selectedProductId = null)
    {
        if (!is_null($selectedProductId)) {
            /** @var ProductCategory $category */
            foreach ($this->productsSet as $category) {
                $p = $category->getProducts() ? $category->getProducts()->toArray() : [];
                if (in_array($selectedProductId, array_map(function(Product $p) { return $p->getId(); }, $p))) {
                    return (int)$category->getId();
                }
            }
        }

        return null;
    }


    /**
     * Return JSON object with dictionary where key equals to {@link Product::id} and value containing an
     * array of {@link TextVersion::id}.
     *
     * @return string JSON string containing a dictionary with structure:
     * {
     *  (int) product_id => [
     *    (int)text_version_1_id, (int)text_version_1_id, ... , (int)text_version_N_id
     *  ]
     * }
     */
    private function getTextVersionMappings()
    {
        $productTextVersionMapping = [];
        /** @var ProductCategory $category */
        foreach ($this->getProductSet() as $category) {
            $p = $category->getProducts() ? $category->getProducts() : [];
            foreach ($p as $product) {
                if (!isset($productTextVersionMapping[$product->getId()])) {
                    $productTextVersionMapping[$product->getId()] = [];
                }
                foreach ($product->getTextVersions() as $textVersion)
                    $productTextVersionMapping[$product->getId()][] = $textVersion->getId();
            }
        }
        return json_encode($productTextVersionMapping);
    }


}
