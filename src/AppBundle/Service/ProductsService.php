<?php

namespace AppBundle\Service;


use AppBundle\Repository\ProductCategoryRepository;

/**
 * Class ProductsService encapsulate all available operations which can be made with Products entity.
 */
class ProductsService
{
    /**
     * Instance of repository class communicating with database.
     *
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;

    /**
     * ProductsService constructor serves as main point for injecting and configuring the service class.
     *
     * @param ProductCategoryRepository $productCategoryRepository Repository class communicating with database.
     */
    public function __construct(ProductCategoryRepository $productCategoryRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
    }


    /**
     * Returns a set of products grouped by {@link ProductCategory}
     */
    public function getAllProductsGroupedByCategory()
    {
        return $this->productCategoryRepository->getAllCategoriesWithEnabledProducts();
    }

}
