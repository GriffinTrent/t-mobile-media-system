<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;

class OrderStateNotifier {

	/** @var \Symfony\Component\DependencyInjection\ContainerInterface */
	private $container;

	private $emailFrom;

	// Its not possible to inject the templating service itself, because it creates cirular reference
	// in this case event the Fabien suggest to inject the whole container as a solution
	// https://github.com/symfony/symfony/issues/2347#issuecomment-2838590
	public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $emailFrom) {
		$this->container = $container;
		$this->emailFrom = $emailFrom;
	}

	public function sendNewOrderEmail(\AppBundle\Entity\Order $order) {
		$adminEmails = $this->getAdminEmails();
		if(!empty($adminEmails)) {
			/** @var $mailer \Swift_Mailer */
			$mailer = $this->container->get('mailer');
			$message = \Swift_Message::newInstance()
				->setSubject("Nová objednávka")
				->setFrom($this->emailFrom)
				->setTo($adminEmails)
				->setBody(
					$this->container->get('templating')->render('AppBundle::Emails/new_order.html.twig', [
						'order' => $order
					]),
					"text/html"
				);
			$mailer->send($message);
		}
	}

	public function sendOrderChangeEmail(\AppBundle\Entity\Order $order) {
		$adminEmails = $this->getAdminEmails();
		if(!empty($adminEmails)) {
			/** @var $mailer \Swift_Mailer */
			$mailer = $this->container->get('mailer');
			$message = \Swift_Message::newInstance()
				->setSubject("Objednávka byla upravena")
				->setFrom($this->emailFrom)
				->setTo($adminEmails)
				->setBody(
					$this->container->get('templating')->render('AppBundle::Emails/order_change.html.twig', [
						'order' => $order
					]),
					"text/html"
				);
			$mailer->send($message);
		}
	}

	public function sendOrderStateChangeEmail(\AppBundle\Entity\Order $order, $oldState) {
		/** @var $mailer \Swift_Mailer */
		$mailer = $this->container->get('mailer');
		$message = \Swift_Message::newInstance()
			->setSubject("Změna stavu objednávky")
			->setFrom($this->emailFrom)
			->setTo($order->getStore()->getUser()->getEmail())
			->setBody(
				$this->container->get('templating')->render('AppBundle::Emails/order_state_change.html.twig', [
					'oldState' => $oldState,
					'order' => $order
				]),
				"text/html"
			);

		$mailer->send($message);
	}

	private function getAdminEmails()
	{
		$qb = $this->container->get('doctrine.orm.entity_manager')->createQueryBuilder();
		$qb->select('u')
			->from(User::class, 'u')
			->where('u.roles LIKE :roles')
			->setParameter('roles', '%ROLE_SUPER_ADMIN%');

		return array_map(function($o) { return $o->getEmail(); }, $qb->getQuery()->getResult());
	}
}
