<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityRepository;

/**
 * Repository communicating with database for ProductCategory Entity.
 */
class ProductCategoryRepository extends EntityRepository
{
    /**
     * Return set of product categories with products that are not disabled.
     *
     * @return ProductCategory[] Set of product categories with products that are not disabled.
     */
    public function getAllCategoriesWithEnabledProducts()
    {
        $qb = $this->createQueryBuilder('pc');
        $qb->select('p, pc, tv, i');
        $qb->innerjoin('pc.products', 'p', 'WITH', 'p.disabled = false');
        $qb->leftjoin('p.textVersions', 'tv');
        $qb->leftjoin('p.image', 'i');

        $results = $qb->getQuery()->getResult();

        return $results;
    }
}
